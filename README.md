# Gruvbox
This is my personal variation of the Gruvbox theme.
You can either configure your plugin manager to install from other sources than github or install it manually:

### Vim

`~/.vim/pack/*/start`

### Neovim

`~/.local/share/nvim/site/pack/*/start`

`*` being a directory name of your choice.

-------

### Dark Palette

![Palette Dark](http://i.imgur.com/wa666xg.png)



Morhetz
--------------

For source, documentation etc. 
[GitHub](https://github.com/morhetz/gruvbox).

License
-------
[MIT/X11][]

   [MIT/X11]: https://en.wikipedia.org/wiki/MIT_License
